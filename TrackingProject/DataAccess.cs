﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;


namespace TrackingProject
{
    public class DataAccess
    {
        // LOAD ALL USERS
        public static List<UserModel> LoadUsers()
        {
            List<UserModel> users = new List<UserModel>();
            using (IDbConnection connection = new SQLiteConnection(LoadConnectionString("Users")))
            {
                var output = connection.Query<UserModel>("select * from User", new DynamicParameters());
                output.ToList();
                users = ParseUserToList(output);
            }
            return users;
        }
        // FIND USERS
        public static List<UserModel> FindUsers(string searchName, bool searchPartName)
        {
            List<UserModel> users = new List<UserModel>();
            using (IDbConnection connection = new SQLiteConnection(LoadConnectionString("Users")))
            {
                if (searchPartName)
                {
                    var output = connection.Query<UserModel>("select * from User where FirstName || ' ' || LastName like '" + searchName + "'");
                    output.ToList();
                    users = ParseUserToList(output);
                }
                else
                {
                    var output = connection.Query<UserModel>("select * from User where FirstName || ' ' || LastName = '" + searchName + "'");
                    output.ToList();
                    users = ParseUserToList(output);
                }
            }
            return users;
        }
        // OUTPUT USERS TO LIST
        private static List<UserModel> ParseUserToList(IEnumerable<UserModel> list)
        {
            List<UserModel> users = new List<UserModel>();
            foreach (var item in list)
            {
                UserModel user = new UserModel()
                {
                    Id = item.Id,
                    FirstName = XORCipher.Decrypt(item.FirstName),
                    LastName = XORCipher.Decrypt(item.LastName),
                    Age = item.Age
                };
                user.UserTracking = LoadTracking(user.Id);
                users.Add(user);
            }
            return users;
        }
        // SAVE USER
        public static int SaveUser(UserModel user)
        {
            int id = 0;
            using (IDbConnection connection = new SQLiteConnection(LoadConnectionString("Users")))
            {
                user.FirstName = XORCipher.Encrypt(user.FirstName.ToLower());
                user.LastName = XORCipher.Encrypt(user.LastName.ToLower());
                id = connection.QuerySingle<int>(@"
                    insert into User (FirstName, LastName, Age) 
                    values (@FirstName, @LastName, @Age);
                    select last_insert_rowid()", user);                
            }
            return id;
        }

        // LOAD TRACKING PER USER
        public static List<TrackingModel> LoadTracking(int userId)
        {
            List<TrackingModel> tracking = new List<TrackingModel>();
            using (IDbConnection connection = new SQLiteConnection(LoadConnectionString("Tracking")))
            {
                var output = connection.Query<TrackingModel>("select * from Tracking where UserId = " + @userId +"");
                output.ToList();
                foreach (var item in output)
                {
                    TrackingModel trackingPoint = new TrackingModel()
                    {
                        Id = item.Id,
                        UserId = item.UserId,
                        CoordX = item.CoordX,
                        CoordY = item.CoordY
                    };
                    tracking.Add(trackingPoint);
                }
            }
            return tracking;
        }
        // SAVE TRACKING
        public static void SaveTracking(TrackingModel tracking)
        {
            using (IDbConnection connection = new SQLiteConnection(LoadConnectionString("Tracking")))
            {
                connection.Execute("insert into Tracking (UserId, CoordX, CoordY) values (@UserId, @CoordX, @CoordY)", tracking);
            }
        }
        // connection string
        private static string LoadConnectionString(string id)
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
