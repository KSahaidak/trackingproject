﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrackingProject
{
    public class InputProcess
    {
        public static void ConsoleRead()
        {
            Console.WriteLine("COMMANDS: ");
            Console.WriteLine("read -k <key> ");
            Console.WriteLine("add -k <key> ");
            Console.WriteLine("find <name / lastname> -k <key> ");
            Console.WriteLine("KEY: 12345");
            Console.WriteLine();
            string input;
            while (true)
            {
                input = Console.ReadLine();
                string[] command = input.Split(" ");
                switch (command[0].ToLower())
                {
                    case "add":
                        AddUser(input);
                        break;
                    case "read":
                        ReadData(input);
                        break;
                    case "find":
                        FindUser(input);
                        break;
                    default:
                        Console.WriteLine("Unknown command.");
                        break;
                }
            }
        }

        // READ DATA
        private static void ReadData(string consoleLine)
        {
            // check if command is correct
            string[] query = consoleLine.Split(" ");
            if (query.Length != 3)
            {
                Console.WriteLine("Incorrect command. Proper syntax: read -k key");
                return;
            }
            else if(query[1].ToLower() != "-k")
            {
                Console.WriteLine("Incorrect command. Proper syntax: read -k key");
                return;
            }
            else if (XORCipher.VerifyKey(query[2]) == false)
            {
                Console.WriteLine("Wrong key.");
                return;
            }

            // load data and print results
            List<UserModel> users = DataAccess.LoadUsers();
            PrintData(users);
            return;
        }

        // FIND DATA
        private static void FindUser(string consoleLine)
        {
            // check if command is correct
            string[] query = consoleLine.Split(" ");
            if (query.Length < 4)
            {
                Console.WriteLine("Incorrect command. Proper syntax: find name/lastname -k key");
                return;
            }
            else if (query[query.Length - 2].ToLower() != "-k")
            {
                Console.WriteLine("Incorrect command. Proper syntax: find name/lastname -k key");
                return;
            }
            else if (XORCipher.VerifyKey(query[query.Length - 1]) == false)
            {
                Console.WriteLine("Wrong key.");
                return;
            }

            // generate search string from input (encode, define if it's a partial search)
            bool searchPartName = false;
            string searchName = "";
            if (query.Length == 4)
            {
                if (query[1].Contains("*"))
                {
                    searchName = XORCipher.Encrypt(query[1].Remove(query[1].Length - 1).ToLower());
                    searchName += "%";
                    searchPartName = true;
                }
                else
                {
                    searchName = XORCipher.Encrypt(query[1].ToLower());
                }
            }

            if (query.Length == 5)
            {
                searchName = XORCipher.Encrypt(query[1].ToLower());
                if (query[2].Contains("*"))
                {
                    searchName += " ";
                    searchName += XORCipher.Encrypt(query[2].Remove(query[2].Length - 1).ToLower());
                    searchName += "%";
                    searchPartName = true;
                }
                else
                {
                    searchName += " ";
                    searchName += XORCipher.Encrypt(query[2].ToLower());
                }
            }

            // search data and print results
            List<UserModel> users = DataAccess.FindUsers(searchName, searchPartName);
            PrintData(users);
            return;
        }

        private static void PrintData(List<UserModel> users)
        {
            if (users.Any())
            {
                foreach (var user in users)
                {
                    Console.WriteLine(user.FullName + " " + user.Age);
                    if (user.UserTracking.Any())
                    {
                        foreach (var track in user.UserTracking)
                        {
                            Console.WriteLine(track.CoordX + " " + track.CoordY);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No data.");
                    }
                }
            }
            else
            {
                Console.WriteLine("No data.");
            }
        }

        // ADD USER
        private static void AddUser(string consoleLine)
        {
            // check if command is correct
            string[] query = consoleLine.Split(" ");
            if (query.Length != 3)
            {
                Console.WriteLine("Incorrect command. Proper syntax: add -k key");
                return;
            }
            else if (query[1].ToLower() != "-k")
            {
                Console.WriteLine("Incorrect command. Proper syntax: add -k key");
                return;
            }
            else if (XORCipher.VerifyKey(query[2]) == false)
            {
                Console.WriteLine("Wrong key.");
                return;
            }

            // input user data
            string input;
            input = Console.ReadLine();
            while (input.ToLower() != "q")
            {
                string[] userinfo = input.Split(" ");
                int age;
                if (userinfo.Length != 3)
                {
                    Console.WriteLine("Incorrect info. Proper syntax: FirstName LastName Age");
                    input = Console.ReadLine();
                }
                else if (int.TryParse(userinfo[2], out age) == false)
                {
                    Console.WriteLine("Incorrect field info: Age");
                    input = Console.ReadLine();
                }
                else
                {
                    UserModel user = new UserModel() { FirstName = userinfo[0], LastName = userinfo[1], Age = age };
                    user.Id = DataAccess.SaveUser(user);
                    AddTracking(user.Id);
                    input = Console.ReadLine();
                }
            }
            return;
        }

        // ADD TRACKING
        private static void AddTracking(int userId)
        {
            string input;
            input = Console.ReadLine();
            while (input.ToLower() != "t")
            {
                string[] trackinginfo = input.Split(" ");
                float coordx, coordy;
                if (trackinginfo.Length != 2)
                {
                    Console.WriteLine("Incorrect fields info.");
                    input = Console.ReadLine();
                }
                else if (!float.TryParse(trackinginfo[0], out coordx) || !float.TryParse(trackinginfo[1], out coordy))
                {
                    Console.WriteLine("Incorrect fields info.");
                    input = Console.ReadLine();
                }
                else
                {
                    TrackingModel track = new TrackingModel() { UserId = userId, CoordX = coordx, CoordY = coordy };
                    DataAccess.SaveTracking(track);
                    input = Console.ReadLine();
                }                
            }
        }


    }
}
