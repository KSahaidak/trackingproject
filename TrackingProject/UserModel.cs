﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackingProject
{
    public class UserModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName 
        { 
            get { return $"{FirstName.ToUpper()} {LastName.ToUpper()}"; } 
        }
        public int Age { get; set; }
        public List<TrackingModel> UserTracking { get; set; } = new List<TrackingModel>();
    }
}
