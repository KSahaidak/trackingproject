﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackingProject
{
    public class XORCipher
    {
        private const string EncryptionKey = "12345";

        // verify key
        public static bool VerifyKey(string key)
        {
            return Equals(key, EncryptionKey);
        }
        
        // generating a key with needed length
        private static string GetRepeatKey(int length)
        {
            var repeatKey = EncryptionKey;
            while (repeatKey.Length < length)
            {
                repeatKey += repeatKey;
            }

            return repeatKey.Substring(0, length);
        }

        private static string Cipher(string text)
        {
            string currentKey = GetRepeatKey(text.Length);
            string result = "";
            for (var i = 0; i < text.Length; i++)
            {
                result += ((char)(text[i] ^ currentKey[i])).ToString();
            }
            return result;
        }

        public static string Encrypt(string plainText)
        {
            return Cipher(plainText);
        }
        public static string Decrypt(string encryptedText)
        {
            return Cipher(encryptedText);
        }

    }
}
