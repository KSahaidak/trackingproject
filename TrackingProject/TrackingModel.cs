﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TrackingProject
{
    public class TrackingModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public float CoordX { get; set; }
        public float CoordY { get; set; }
    }
}
